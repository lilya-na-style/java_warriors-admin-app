package com.project;
import com.project.caches.FactoryCache;
import com.project.services.factories.Factory;
import com.project.services.factories.environments.*;
import com.project.services.factories.UserDialog;

import java.io.IOException;

import java.util.Scanner;

public class Main {


    public static void main(String[] args) throws IOException, ClassNotFoundException {

// массив фабрик: хмл, ямл и тд
  Factory[] factories = {new BinaryReader(), new CsvReader(), new XMLReader(), new YAMLReader()};
        //{new XmlImpl(), new JsonImpl(), new BinaryImpl(), new CsvImpl(), new YamlImpl()};
        //создаем объект факториКеша, куда аргументом отдаем массив фабрик
        FactoryCache factoryCache = new FactoryCache(factories);

        Scanner scanner = new Scanner(System.in);
        new UserDialog(scanner, factoryCache).start();



        //     String s = new FileProcessor(new WrapperFile()).start("src/resources/file.txt");
        //      System.out.println(s);
    }
}
