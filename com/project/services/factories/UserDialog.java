package com.project.services.factories;

import com.project.caches.FactoryCache;
import com.project.services.factories.Factory;

import java.io.IOException;
import java.util.Scanner;

public class UserDialog {

    private static final String READ_ALL = "readAll";
    private static final String READ_BY_ID = "readById";
    private static final String CREATE = "create";
    private static final String DELETE = "delete";
    private static final String UPDATE = "update";
    private static final String SWITCH_EXTENSION = "switch";
    private static final String HELP = "help";
    private static final String EXIT = "exit";
    private static final String HELP_MSG =
            ("С помощью этой программы вы можете вносить и получать клиентские данные.\n1) получить данные " +
            "обо всех пользователях введите readAll;\n2) получить информацию о пользователе введите readById; " +
            "\n3) изменить пользователя - update;\n4) удалить пользователя - delete;\n5) create - создать нового пользователя.");
    private FactoryCache factoryCache;
    private Scanner scanner;
    private Factory factory;
    boolean exit;
    boolean help;

    public UserDialog(Scanner scanner, FactoryCache factoryCache) {
        this.scanner = scanner;
        this.factoryCache = factoryCache;
    }

    public UserDialog(){

    }

    public void start() throws IOException, ClassNotFoundException {
        System.out.println("Привет! Это админское приложение для работы с базами данных клиентов!");


        selectEnvironment();
        if (factory == null) {
            System.out.println("Неправильно введенные данные!");
            selectEnvironment();
            System.out.println("Длы выхода введите: exit, для справки: help");

        }


        while (true) {
            System.out.println("Пожалуйста, выберите команду: ");
            String command = scanner.next();
            processCommand(command);
        }
    }

    private void processCommand(String command) throws IOException, ClassNotFoundException {
        switch (command) {
            case (CREATE) :
                factory.create();
                break;
            case (UPDATE) :
                factory.update(1);
                break;
            case (DELETE) :
                factory.deletePerson(1);
                break;
            case (READ_ALL) :
                factory.readAll();
                break;
            case (READ_BY_ID) :
                factory.readById(0);
                break;
            case (SWITCH_EXTENSION) :
                selectEnvironment();
                break;
            case (HELP) :
                helpForUser();
                break;
            case (EXIT) :
                exitApp();
            case "cr" :
                factory.createAlongWith();
                break;
            default:
                System.out.println("Данные неверны! Для вызова помощника введите: 'help'");
                break;
        }
    }

    public void selectEnvironment() throws IOException, ClassNotFoundException {
        System.out.println("Для дальнейшей работы выберите новое окружение: ");
        String environment = scanner.next();
        factory = factoryCache.getEnvironment(environment);
        if(environment.equals("exit")) {
            exitApp();
        }
       if(environment.equals("help")){
            helpForUser();
        }

    }

     public void exitApp(){
            System.out.println("Вы действительно хотите закрыть приложение?. Введите '+', если да");
            scanner.nextLine();
            if (scanner.nextLine().equals("+")) {
                System.out.println("Приложение закрывается. До встречи!");
            }
         System.exit(0);
     }

      public void helpForUser(){
             System.out.println(HELP_MSG);


        }


    }

