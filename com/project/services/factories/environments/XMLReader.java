package com.project.services.factories.environments;
import com.project.models.Person;
import com.project.services.factories.Factory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Scanner;


public class XMLReader implements Factory {
    private static Document doc;
    private static Node rootNode;
    private static NodeList rootChildes;
    private static NodeList atributs;
    private static Scanner scanner = new Scanner(System.in);
    private static Scanner scanner2 = new Scanner(System.in);

@Override
    public void readAll(){
        connectXML();
        System.out.println("ID  FirstName  LastName Age City");
        for (int i=0;i<rootChildes.getLength();i++)
        {
            atributs = rootChildes.item(i).getChildNodes();
            for (int j=0;j< atributs.getLength();j++)
            {
                System.out.print(atributs.item(j).getTextContent()+"  ");
            }
            System.out.println();
        }
    }

    @Override
    public void readById(int id){
        System.out.println("Введите ID которое хотите вывести ");
        id  = scanner.nextInt();
        connectXML();
        System.out.println("ID  FirstName  LastName Age City");
        atributs = rootChildes.item(id).getChildNodes();
        for (int j=0; j<atributs.getLength(); j++)
        {
            System.out.print(atributs.item(j).getTextContent()+"  ");
        }
        System.out.println();
    }

    @Override
    public void update() throws IOException, ClassNotFoundException {

    }


    @Override
    public boolean deletePerson(int id){
        System.out.println("Введите ID которое хотите удалить ");
        id  = scanner.nextInt();
        connectXML();
        rootNode.removeChild(rootChildes.item(id));
        writeDocument(doc);
        System.out.println("Удаление прошло успешно!");

        return false;
    }

    @Override
    public void update(int id){
        System.out.println("Введите ID которое хотите обновить ");
        id  = scanner.nextInt();
        System.out.print("Введите какое поле вы хотите обновить (FirstName LastName Age City) \n ");
        String field  = scanner2.nextLine();
        System.out.print("Введите новое поле для "+ field+" ");
        String newText  = scanner2.nextLine();
        connectXML();
        System.out.println("ID  FirstName  LastName Age City");
        atributs = rootChildes.item(id).getChildNodes();

        switch (field) {
            case ("FirstName"):
                atributs.item(1).setTextContent(newText);
                break;
            case ("LastName"):
                atributs.item(2).setTextContent(newText);
                break;
            case ("Age"):
                atributs.item(3).setTextContent(newText);
                break;
            case ("City"):
                atributs.item(4).setTextContent(newText);
                break;
            default:
                System.out.println("Error updateXML in XMLReader try again");
                update(id);
                break;
        }
        System.out.println();
        writeDocument(doc);
        System.out.println("Обновление прошло успешно!");
    }

    public static void connectXML(){
        File file = new File("src/recources/ClientsBase.xml");
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        {
            try {
                doc = dbf.newDocumentBuilder().parse(file);


            } catch (Exception e) {
                System.out.println("Open parsing error " + e.toString());
            }
        }
        rootNode = doc.getFirstChild();
        rootChildes = rootNode.getChildNodes();
    }



@Override
    public void create(){
        System.out.println("Введите ID ");
        int id  = scanner.nextInt();
        System.out.print("Введите FirstName\n");
        String FirstName  = scanner2.nextLine();
        System.out.println("Введите LastName ");
        String LastName  = scanner2.nextLine();
        System.out.println("Введите Age ");
        int age  = scanner.nextInt();
        System.out.print("Введите city \n");
        String city  = scanner2.nextLine();
        Person person = new Person(id, FirstName, LastName, age, city);

        Node root  = doc.getDocumentElement();


        Element personXML = doc.createElement("Person");

        Element personidXML = doc.createElement("ID");
        personidXML.setTextContent(String.valueOf(person.getId()));

        Element personFirstNameXML = doc.createElement("FirstName");
        personFirstNameXML.setTextContent(person.getFirstName());

        Element personLastNameXML = doc.createElement("LastName");
        personLastNameXML.setTextContent(person.getLastName());

        Element personAgeXML = doc.createElement("Age");
        personAgeXML.setTextContent(String.valueOf(person.getAge()));

        Element personCityXML = doc.createElement("City");
        personCityXML.setTextContent(person.getCity());

        personXML.appendChild(personidXML);
        personXML.appendChild(personFirstNameXML);
        personXML.appendChild(personLastNameXML);
        personXML.appendChild(personAgeXML);
        personXML.appendChild(personCityXML);

        root.appendChild(personXML);

        writeDocument(doc);

    }



    private static void writeDocument(Document document) throws TransformerFactoryConfigurationError {
        try {
            Transformer tr = TransformerFactory.newInstance().newTransformer();
            DOMSource source = new DOMSource(document);
            FileOutputStream fos = new FileOutputStream("src/recources/ClientsBase.xml");
            StreamResult result = new StreamResult(fos);
            tr.transform(source, result);
        } catch (TransformerException | IOException e) {
            e.printStackTrace(System.out);
        }
    }




    @Override
    public Person[] readRPersons() throws IOException, ClassNotFoundException {
        return new Person[0];
    }

    @Override
    public void updatePerson(Person person) {

    }

    @Override
    public void createPerson(Person person) throws IOException {

    }


    @Override
    public void createAlongWith() throws IOException, ClassNotFoundException {

    }

}
