package com.project.services.factories.environments;

import com.project.models.Person;
import com.project.services.factories.Factory;
import java.io.FileInputStream;
import java.io.IOException;
import org.yaml.snakeyaml.DumperOptions;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.nodes.Tag;
import org.yaml.snakeyaml.representer.Representer;
import java.io.PrintWriter;
import java.util.Scanner;


public class YAMLReader implements Factory {

    private final Scanner scanner = new Scanner(System.in);
    String filePath = "src/recources/ClientsBase.yaml";

    @Override
    public void create() throws IOException {
        try {
            Person[] persons = new Person[]{
                    new Person(1, "s", "s", 0, "s")
            };
            DumperOptions options = new DumperOptions();
            options.setIndent(2);
            options.setPrettyFlow(true);
            options.setDefaultFlowStyle(DumperOptions.FlowStyle.BLOCK);
            Representer representer = new Representer();
            representer.addClassTag(Person[].class, Tag.MAP);
            Yaml yaml = new Yaml(representer, options);
            PrintWriter writer = new PrintWriter(filePath);
            yaml.dump(persons, writer);
            writer.close();
        } catch (Exception ex) {
            System.out.println(ex.toString());
        }
        WriteYaml();

    }

    //create

    @Override
    public boolean deletePerson(int id) throws IOException {


        FileInputStream gpadata = new FileInputStream(filePath);

        DumperOptions options = new DumperOptions();
        options.setIndent(2);
        options.setPrettyFlow(true);
        options.setDefaultFlowStyle(DumperOptions.FlowStyle.BLOCK);
        Representer representer = new Representer();
        representer.addClassTag(Person[].class, Tag.MAP);
        Yaml yaml = new Yaml(representer, options);
        Person[] data = yaml.loadAs(gpadata, Person[].class);

        for (; ;) {
            Scanner scanner = new Scanner(System.in);
            System.out.println("Если хотите удалить пользователя - введите ID");
            id = scanner.nextInt();
            int length = data.length ;
            if (length < id) {
                System.out.println("Пользователь с таким ID не найден");
                continue;
            }
            break;
        }
        Integer personsArrayId = null;
        for (int i = 0; i < data.length; i++) {
            Person person = data[i];
            if (person != null && person.getId() == id) {
                System.out.println("Пользователь с таким ID найден");
                personsArrayId = i;
                break;
            }
        }
        if (personsArrayId == null) {
            System.out.println("Пользователь с таким ID не найден");
            return false;
        }
        for (int i = personsArrayId; i < data.length - 1; i++) {
            data[personsArrayId] = data[personsArrayId + 1];
        }

        PrintWriter writer = new PrintWriter(filePath);
        yaml.dump(data, writer);
        writer.close();
        return false;

    }

    //delete

    @Override
    public void update(int id) throws IOException {


        String userReader = "";
        FileInputStream gpadata = new FileInputStream(filePath);
        DumperOptions options = new DumperOptions();
        options.setIndent(2);
        options.setPrettyFlow(true);
        options.setDefaultFlowStyle(DumperOptions.FlowStyle.BLOCK);
        Representer representer = new Representer();
        representer.addClassTag(Person[].class, Tag.MAP);
        Yaml yaml = new Yaml(representer, options);
        Person[] persons = yaml.loadAs(gpadata, Person[].class);


        for (; ; ) {
            Scanner scanner = new Scanner(System.in);
            System.out.println("Если хотите изменить данные пользователя - введите ID\"");
            id = Integer.parseInt(scanner.next());
            int length = persons.length;
            if (length < id) {
                System.out.println("Error");
                continue;
            }
            break;
        }
        Integer personsArrayId = null;
        for (int i = 0; i < persons.length; i++) {
            Person person = persons[i];
            if (person != null && person.getId() == id) {
                personsArrayId = i;
                break;
            }
        }
        if (personsArrayId == null) {
            System.out.println("Пользователь с таким ID не найден");
            return;
        }

        Person person = new Person(id, "", "", 0, "");
        System.out.println("Введите новое имя");
        userReader = scanner.next();
        person.setFirstName(userReader);
        System.out.println("Введите новую фамилию");
        userReader = scanner.next();
        person.setLastName(userReader);
        System.out.println("Введите новый возраст");
        int number1 = scanner.nextInt();
        if (0 < number1 && 100 > number1)
            person.setAge(number1);
        else
            System.out.println("Error");
        System.out.println("Введите новый город");
        userReader = scanner.next();
        person.setCity(userReader);

        persons[personsArrayId] = person;
        System.out.println(persons[personsArrayId]);


        PrintWriter writer = new PrintWriter(filePath);
        yaml.dump(persons, writer);
        writer.close();
    }

//update
 @Override
    public void readAll() {
        try(FileInputStream fin=new FileInputStream(filePath))
        {
            System.out.printf("File size: %d bytes \n", fin.available());

            int i=-1;
            while((i=fin.read())!=-1){

                System.out.print((char)i);
            }
        }
        catch(IOException ex){

            System.out.println(ex.getMessage());
        }

    }
@Override
    public void readById(int id) throws IOException {

        FileInputStream gpadata = new FileInputStream(filePath);
        DumperOptions options = new DumperOptions();
        options.setIndent(2);
        options.setPrettyFlow(true);
        options.setDefaultFlowStyle(DumperOptions.FlowStyle.BLOCK);
        Representer representer = new Representer();
        representer.addClassTag(Person[].class, Tag.MAP);
        Yaml yaml = new Yaml(representer, options);
        Person[] data = yaml.loadAs(gpadata, Person[].class);


        for (int k = 0; ; k++) {
            Scanner scanner = new Scanner(System.in);
            System.out.println("Введите ID");
            id = Integer.parseInt(scanner.next());
            int length = data.length ;
            if (length < id) {
                System.out.println("Error");
                continue;
            }
            break;
        }
        for (int i = 0; i < data.length; i++) {
            Person person = data[i];
            if (person != null && person.getId() == id) {
                String strToPrint = "ID = " + (id) + "\n" + data[id - 1];
                System.out.println(strToPrint);
                break;
            }
        }
    }



    public void WriteYaml() throws IOException {

        FileInputStream gpadata = new FileInputStream(filePath);

        DumperOptions options = new DumperOptions();
        options.setIndent(2);
        options.setPrettyFlow(true);
        options.setDefaultFlowStyle(DumperOptions.FlowStyle.BLOCK);
        Representer representer = new Representer();
        representer.addClassTag(Person[].class, Tag.MAP);
        Yaml yaml = new Yaml(representer, options);
        Person[] data = yaml.loadAs(gpadata, Person[].class);

        final Scanner scanner = new Scanner(System.in);
        String userReader = "";

        System.out.println("Введите количество пользователей которых вы хотите ввести");
        int number = Integer.parseInt(scanner.next());
        Person[] persons = new Person[data != null ? data.length + number - 1 : number];
        if (data != null) {
            System.arraycopy(data, 0, persons, 0, data.length);
        }
int id = 1;
//id++
        for (int i = data != null ? data.length - 1 : 0; i < persons.length; i++) {
            persons[i] = new Person(id++, "", "", 0, "");
            //System.out.println("Введите id");
         //   userReader = scanner.nextInt();
            System.out.println("Введите новое имя");
            userReader = scanner.next();
            persons[i].setFirstName(userReader);
            System.out.println("Введите новую фамилию");
            userReader = scanner.next();
            persons[i].setLastName(userReader);
            System.out.println("Введите новый возраст");

            int number1 = scanner.nextInt();
            if (0 < number1 && 100 > number1)
                persons[i].setAge(number1);
            else
                System.out.println("Error");
            System.out.println("Введите новый город");
            userReader = scanner.next();
            persons[i].setCity(userReader);

            System.out.println(persons[i]);
        }

        PrintWriter writer = new PrintWriter(filePath);
        yaml.dump(persons, writer);
        writer.close();
    }


    @Override
    public Person[] readRPersons() {
        System.out.println("yaml read");
        return new Person[]{new Person(11, "Data"), new Person(9, "Baba")};
    }

    @Override
    public void updatePerson(Person person) {

    }

    @Override
    public void createPerson(Person person) {
        System.out.println("yaml create " + person);
    }




    @Override
    public void update() throws IOException, ClassNotFoundException {

    }


    @Override
    public void createAlongWith() throws IOException, ClassNotFoundException {

    }


}
