package com.project.services.factories.environments;

import com.project.models.Person;
import com.project.services.factories.Factory;

import java.io.*;
import java.util.Scanner;


public class BinaryReader implements Factory {
    String filePath = "src/recources/ClientsBase.bin";
    FileOutputStream fos = new FileOutputStream(filePath);
    ObjectOutputStream os = new ObjectOutputStream(fos);
    Person[] listOfPerson = new Person[10];

    public BinaryReader() throws IOException {
    }

    @Override
    public void readAll() throws IOException, ClassNotFoundException {
        FileInputStream fis = new FileInputStream(filePath);
        ObjectInputStream ois = new ObjectInputStream(fis);
        Person [] listOfPerson = (Person[]) ois.readObject();
        Scanner sc = new Scanner(System.in);
        System.out.println("Если хотите получить данные всех пользователей нажмите: '+'");
        String userReader = sc.nextLine();
        int j = 0;
        for (j = 0; j < listOfPerson.length; j++) {
            if (userReader.equals("+")) {
                //    System.out.println(cba[j].name + " " + cba[j].age + " " + cba[j].city);
                System.out.println(listOfPerson[j]);
            }
        }

        fis.close();
        ois.close();

    }

    @Override
    public void createAlongWith() throws IOException, ClassNotFoundException {
        //проверяем есть ли объекты в файле
        //если файл не пустой считываем их в массив
        //определяем длину массива
        //копируем текущие объекты в новый массив + объекты считанные с консоли

        Person[] listOfPerson = new Person[10];
        create();
        FileInputStream fis = new FileInputStream(filePath);
       ObjectInputStream ois = new ObjectInputStream(fis);
       listOfPerson = (Person[]) ois.readObject();
        //  if(fis.read() != -1){
        //      System.out.println("file is not empty");
        //  }
        int readDataLength = 0;
        int count = 0;
        int i = 0;
        int j = 0;
        for (i = 0; i < listOfPerson.length; i++) {
            // i = count + j;
            readDataLength = listOfPerson.length;
        }
        if (readDataLength < 0) {
            System.out.println("Ваш список пуст");
        } else {
           System.out.println("Сейчас в вашей базе " + readDataLength + " пользователя");
        }
        FileOutputStream fos = new FileOutputStream(filePath);
        ObjectOutputStream os = new ObjectOutputStream(fos);

        Scanner sc = new Scanner(System.in);
        Scanner s = new Scanner(System.in);
        String name, lname, city = null;
        int id, age = 0;
        String c = null;
        System.out.println("Введите ID");
        id = sc.nextInt();
        System.out.print("Введите имя\n");
        name = sc.next();
        System.out.print("Введите фамилию\n");
        lname = sc.next();
        System.out.print("Введите возраст\n");
        age = s.nextInt();
        System.out.println("Введите город");
        city = sc.next();
        int newLength = readDataLength + 1;
        Person[] newPersonList = new Person[newLength];
        newPersonList[0] = new Person(id, name, lname, age, city);
        for (j = 0; j < newPersonList.length; j++) {
            //   newPersonList[j].setId(newLength+1))+ " " +
            //   System.out.println();
            System.arraycopy(listOfPerson, 4, newPersonList, 1, 5);
            os.writeObject(newPersonList);


        }
    }

    @Override
    public void create() throws IOException {
        FileOutputStream fos = new FileOutputStream(filePath);
        ObjectOutputStream os = new ObjectOutputStream(fos);
        //определяем начальный массив объектов
        Person[] listOfPerson = new Person[10];
        listOfPerson[0] = new Person(0, "Kate", "Jay", 2, "ny");
        listOfPerson[1] = new Person(1, "Molly", "Mur", 3, "la");
        listOfPerson[2] = new Person(2, "Lily", "Trump", 4, "kh");

        for (int i = 0; i < listOfPerson.length; i++) {
            os.writeObject(listOfPerson);
        }


        fos.close();
        os.close();

    }


    @Override
    public void readById(int id) throws IOException, ClassNotFoundException {
        Scanner sc = new Scanner(System.in);
        FileInputStream fis = new FileInputStream(filePath);
        ObjectInputStream ois = new ObjectInputStream(fis);
       // if(fis.read() == -1){
          //  System.out.println("Ошибка: файл пуст");
            System.out.println("Если хотите получить данные пользователя - введите ID");
       // }
       // else {

            id = sc.nextInt();
            int j = 0;
            Person[] listOfPerson = (Person[]) ois.readObject();
            for (j = 0; j < listOfPerson.length; j++) {
                if (id != j) {
                    System.out.println("Пользователя с таким ИД не существует");
                }
                else {
                    System.out.println(listOfPerson[j].toString());

                }

            }

        fis.close();
        ois.close();

    }

    @Override
    public void update() throws IOException, ClassNotFoundException {

    }


    @Override
    public boolean deletePerson(int id) throws IOException, ClassNotFoundException {
        FileInputStream fis = new FileInputStream(filePath);
        ObjectInputStream ois = new ObjectInputStream(fis);
        Person[] listOfPerson = (Person[]) ois.readObject();
        Scanner sc = new Scanner(System.in);
        System.out.println("Если хотите получить данные пользователя - введите ID");
        id = sc.nextInt();
        int j = 0;
        for (j = 0; j < listOfPerson.length; j++) {
            if (id == j) {
                System.out.println(listOfPerson[id].toString());
                FileOutputStream fos = new FileOutputStream(filePath);
                ObjectOutputStream os = new ObjectOutputStream(fos);
                os.writeObject(listOfPerson[id] = null);
                System.out.println("Пользователь удален");
                //create();
            }
        }

        fis.close();
        ois.close();
        return true;
    }

    private static final String NAME = "name";
    private static final String LNAME = "lastname";
    private static final String AGE = "age";
    private static final String CITY = "city";

    @Override
    public void update(int id) throws IOException, ClassNotFoundException {
            FileInputStream fis = new FileInputStream(filePath);
            ObjectInputStream ois = new ObjectInputStream(fis);
            Person listOfPerson [] = (Person[]) ois.readObject();
            Scanner sc = new Scanner(System.in);
            System.out.println("Введите ИД пользователя, которого хотите изменить");
            int userReader = sc.nextInt();
            int j = 0;
            for (j = 0; j < listOfPerson.length; j++) {
                if (userReader == j) {
                    System.out.println("Пользователь с таким ИД существует!");
                    System.out.println("Данные вашего пользователя: ");
                    System.out.println(listOfPerson[j].getFirstName() + " " + listOfPerson[j].getLastName() +
                            " " + listOfPerson[j].getAge() + " " + listOfPerson[j].getCity());
                    FileOutputStream fos = new FileOutputStream(filePath);
                    ObjectOutputStream os = new ObjectOutputStream(fos);
                    //os.writeObject(cba);
                    //os.writeBytes("Пользователь удален");
                    System.out.println("Для того, чтобы изменить имя, введите: name, город - city, возраст - age");
                    String userUpdate = sc.next();
                    int count = 0;
                    while (count < 1) {
                        switch (userUpdate) {
                            case (NAME):
                                System.out.println("Введите новое имя");
                                String update = sc.nextLine();
                                listOfPerson[j].setFirstName(update);
                                System.out.println("Спасибо изменения внесены!");
                                os.writeObject(listOfPerson);
                                readAll();
                                System.exit(0);
                            case (LNAME):
                                System.out.println("Введите новую фамилию");
                                String update2 = sc.nextLine();
                                listOfPerson[j].setLastName(update2);
                                System.out.println("Спасибо изменения внесены!");
                                os.writeObject(listOfPerson);
                                readAll();
                                System.exit(0);
                            case (AGE):
                                System.out.println("Введите новый возраст");
                                int upd = sc.nextInt();
                                listOfPerson[j].setAge(upd);
                                System.out.print("Спасибо изменения внесены!");
                                os.writeObject(listOfPerson);
                                readAll();
                                System.exit(0);
                            case (CITY):
                                System.out.println("Введите новый город");
                                update = sc.nextLine();
                                listOfPerson[j].setCity(update);
                                System.out.print("Спасибо изменения внесены!");
                                os.writeObject(listOfPerson);
                                readAll();
                                System.exit(0);
                            default:
                                System.out.println("Вы ввели неверные данные!");
                                System.exit(0);

                        }

                    }


                    // System.out.println(cba[j].name + " " + cba[j].age + " " + cba[j].city);
                    //break;
                }
                // else{
                //    System.out.println("Пользователя с таким ИД не существует");
                //    break;
                //}
            }
            fis.close();
            ois.close();


        }





    @Override
    public Person[] readRPersons() throws IOException, ClassNotFoundException {
        return new Person[0];
    }

    @Override
    public void updatePerson(Person person) {


    }

    @Override
    public void createPerson(Person person) throws IOException {

    }


}
























    /*
    @Override
    public Person[] readRPersons() {
        System.out.println("binary read");
        return new Person[]{new Person(1, "Valera"), new Person(3, "Gleb")};
    }

    @Override
    public void updatePerson(Person person) {
        System.out.println("binary update " + person);
    }

    @Override
    public void createPerson(Person person) {
        System.out.println("binary create " + person);
    }

    @Override
    public boolean deletePerson(int id) {
        System.out.println("binary delete " + id);
        return false;
    }

    @Override
    public void readAll() {

    }

     */
