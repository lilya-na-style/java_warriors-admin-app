package com.project.services.factories;
import com.project.models.Person;
import java.io.FileNotFoundException;
import java.io.IOException;

public interface Factory {

    Person[] readRPersons() throws IOException, ClassNotFoundException;

    void updatePerson(Person person);

    void createPerson(Person person) throws IOException;

    boolean deletePerson(int id) throws FileNotFoundException, IOException, ClassNotFoundException;

    void readAll() throws IOException, ClassNotFoundException;

    void readById(int id) throws IOException, ClassNotFoundException;

   void update() throws IOException, ClassNotFoundException;

    void create() throws IOException, ClassNotFoundException;

    void update(int id) throws IOException, ClassNotFoundException;

    void createAlongWith() throws IOException, ClassNotFoundException;




}
