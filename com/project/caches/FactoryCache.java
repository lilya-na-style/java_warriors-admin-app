package com.project.caches;

import com.project.services.factories.environments.CommandServiceJson;
import com.project.services.factories.Factory;

import java.io.IOException;

public class FactoryCache {

    private Factory[] factories;

    public FactoryCache(Factory[] factories)
    {
        this.factories = factories;
    }

    public Factory getEnvironment(String factory) throws IOException, ClassNotFoundException {
        switch (factory) {

            case "binary" :
                return factories[0];
            case "csv" :
                return factories[1];
            case "xml" :
                return factories[2];
            case "yaml" :
                return factories[3];
            case "json" :
              new CommandServiceJson().userInterface();
                return factories[4];

            default:
                return null;
        }

    }
}
